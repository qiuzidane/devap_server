package main

import (
	"database/sql"
	_ "database/sql"
	"fmt"
	_ "fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"time"
	_ "time"
)
type getRank interface {

}
type user struct {
	userId   string
	userName string
	times 	 int
}
type userRank struct {
	Users []user
}

type project struct {
	ProjectId   string
	ProjectName string
	times 	 int
}
type projectRank struct {
	Project []project
}

type language struct {
	languageName string
	percent 	 float32
	times 		 float32
}
type languageRank struct {
	Language []language
}

func (p*projectRank)Rank(db *sql.DB){
	time:=time.Now()
	nowtime:=time.String()[:19]
	//fmt.Print(nowtime,"\n")
	time=time.AddDate(0,0,-3)
	befortime:=time.String()[:19]
	//fmt.Print(befortime+"\n")

	querysen:="select b.project_id,b.project_name,COUNT(b.project_id) as times " +
		"from git_tbl as a join project_tbl as b " +
		"on a.project_id=b.project_id and " +
		"a.submission_date>='"+befortime+"' and a.submission_date<='"+nowtime+
		"' GROUP BY b.project_id ORDER BY COUNT(b.project_id);"

	//fmt.Print(querysen)
	rows:=query(querysen,db)

scan:
	if rows.Next() {
		//columns, err := rows.Columns()
		//fmt.Println(columns)
		_pro := new(project)
		err := rows.Scan(&_pro.ProjectId, &_pro.ProjectName,&_pro.times)
		if err != nil {
			log.Fatal(err.Error())

		}
		//fmt.Println(person.userId, person.userName,person.times)
		p.Project= append(p.Project, *_pro)
		//fmt.Print(user_res)
		goto scan
	}
}

func (l*languageRank)Rank(db *sql.DB){
	time:=time.Now()
	nowtime:=time.String()[:19]
	//fmt.Print(nowtime,"\n")
	time=time.AddDate(0,0,-3)
	befortime:=time.String()[:19]
	//fmt.Print(befortime+"\n")

	querysen:="select b.language,COUNT(b.language) as times " +
		"from git_tbl as a join project_tbl as b " +
		"on a.project_id=b.project_id and " +
		"a.submission_date>='"+befortime+"' and a.submission_date<='"+nowtime+
		"' GROUP BY b.project_id ORDER BY COUNT(b.language);"

	//fmt.Print(querysen)
	rows:=query(querysen,db)
	var  count float32
scan:
	if rows.Next() {
		//columns, err := rows.Columns()
		//fmt.Println(columns)
		lang := new(language)
		err := rows.Scan(&lang.languageName, &lang.times)
		count+=lang.times
		if err != nil {
			log.Fatal(err.Error())

		}
		//fmt.Println(person.userId, person.userName,person.times)
		l.Language= append(l.Language, *lang)
		//fmt.Print(user_res)
		goto scan
	}
	for i:=0;i<len(l.Language);i++ {
		l.Language[i].percent= float32(l.Language[i].times / count)
	}
}

func (u*userRank)Rank(db *sql.DB){
	time:=time.Now()
	nowtime:=time.String()[:19]
	//fmt.Print(nowtime,"\n")
	time=time.AddDate(0,0,-3)
	befortime:=time.String()[:19]
	//fmt.Print(befortime+"\n")

	querysen:="select b.user_id,b.user_name,COUNT(b.user_id) as times " +
		"from git_tbl as a join user_tbl as b " +
		"on a.user_id=b.user_id and " +
		"a.submission_date>='"+befortime+"' and a.submission_date<='"+nowtime+
		"' GROUP BY b.user_id ORDER BY COUNT(b.user_id);"

	//fmt.Print(querysen)
	rows:=query(querysen,db)

scan:
	if rows.Next() {
		//columns, err := rows.Columns()
		//fmt.Println(columns)
		person := new(user)
		err := rows.Scan(&person.userId, &person.userName,&person.times)
		if err != nil {
			log.Fatal(err.Error())

		}
		//fmt.Println(person.userId, person.userName,person.times)
		u.Users= append(u.Users, *person)
		//fmt.Print(user_res)
		goto scan
	}
}

func query(sql_statment string, db *sql.DB) *sql.Rows {

	rows,err := db.Query(sql_statment)
	if err != nil {
		fmt.Print(err.Error())
		return nil
	}
	return rows
}

func linkdata(Source string) *sql.DB {
	db,err:=sql.Open("mysql",Source)
	if err != nil {
		fmt.Print(err.Error())
		return nil
	}else {
		fmt.Print("link success")
	}
	return db
}

func main()  {
	//连接数据库
	Source:="root:Root12345679!@#@tcp(47.113.113.225)/git_database"
	db:=linkdata(Source)
	u:=userRank{}//生成user对象
	u.Rank(db)//查询数据库并生成排名按顺序放到userrank中的数组，内容是{用户id，用户名，用户提交量}
	fmt.Print("user rank:",u.Users,"\n")

	p:=projectRank{}//生成project对象
	p.Rank(db)//查询数据库并生成排名按顺序放到projectrank中的数组，内容是{项目id，项目名，项目提交量}
	fmt.Print("project rank:",p.Project)

	l:=languageRank{}
	l.Rank(db)//查询数据库并生成排名按顺序放到languagerank中的数组，内容是{语言名，语言提交量，语言占比}
	fmt.Print("Language rank:",l.Language)
}